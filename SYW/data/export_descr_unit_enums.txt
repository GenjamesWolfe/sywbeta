;***********************AUSTRIA****************************

austrian_line
austrian_line_descr
austrian_line_descr_short

Hungarian_fusiliers
Hungarian_fusiliers_descr
Hungarian_fusiliers_descr_short

austrian_grenadiers
austrian_grenadiers_descr
austrian_grenadiers_descr_short

Hungarian_grenadiers
Hungarian_grenadiers_descr
Hungarian_grenadiers_descr_short

Swabian_circle_fusiliers
Swabian_circle_fusiliers_descr
Swabian_circle_fusiliers_descr_short

Wurzburg_fusiliers
Wurzburg_fusiliers_descr
Wurzburg_fusiliers_descr_short

;***********************PRUSSIA****************************

prussian_landmilitz
prussian_landmilitz_descr
prussian_landmilitz_descr_short

prussian_garrison
prussian_garrison_descr
prussian_garrison_descr_short

prussian_line
prussian_line_descr
prussian_line_descr_short

prussian_fusiliers
prussian_fusiliers_descr
prussian_fusiliers_descr_short

prussian_grenadiers
prussian_grenadiers_descr
prussian_grenadiers_descr_short

prussian_leib
prussian_leib_descr
prussian_leib_descr_short

;***********************Russia****************************

Russian_fusiliers
Russian_fusiliers_descr
Russian_fusiliers_descr_short

russian_grenadiers
russian_grenadiers_descr
russian_grenadiers_descr_short

Russian_footguard
Russian_footguard_descr
Russian_footguard_descr_short


;***********************Britain******************************

british_lights
british_lights_descr
british_lights_descr_short

provincial_line
provincial_line_descr
provincial_line_descr_short

rangers
rangers_descr
rangers_descr_short

eic_foot
eic_foot_descr
eic_foot_descr_short

british_line
british_line_descr
british_line_descr_short

british_highlander
british_highlander_descr
british_highlander_descr_short

british_grenadiers
british_grenadiers_descr
british_grenadiers_descr_short

british_footguard
british_footguard_descr
british_footguard_descr_short

british_fusiliers
british_fusiliers_descr
british_fusiliers_descr_short

british_marines
british_marines_descr
british_marines_descr_short

hanovarian_line
hanovarian_line_descr
hanovarian_line_descr_short


;***********************Sweden******************************

swedish_line
swedish_line_descr
swedish_line_descr_short

finnish_line
finnish_line_descr
finnish_line_descr_short

swedish_varvade_line
swedish_varvade_line_descr
swedish_varvade_line_descr_short

swedish_grenadiers
swedish_grenadiers_descr
swedish_grenadiers_descr_short

swedish_foot_guard
swedish_foot_guard_descr
swedish_foot_guard_descr_short

;***********************Spain******************************

spanish_migueletes
spanish_migueletes_descr
spanish_migueletes_descr_short

spanish_militia
spanish_militia_descr
spanish_militia_descr_short

spanish_fusiliers
spanish_fusiliers_descr
spanish_fusiliers_descr_short

spanish_italian_fusiliers
spanish_italian_fusiliers_descr
spanish_italian_fusiliers_descr_short

spanish_irish_fusiliers
spanish_irish_fusiliers_descr
spanish_irish_fusiliers_descr_short

spanish_swiss_fusiliers
spanish_swiss_fusiliers_descr
spanish_swiss_fusiliers_descr_short

spanish_grenadiers
spanish_grenadiers_descr
spanish_grenadiers_descr_short

guardias
guardias_descr
guardias_descr_short

;***********************Saxony******************************

saxon_fusiliers
saxon_fusiliers_descr
saxon_fusiliers_descr_short

saxon_grenadiers
saxon_grenadiers_descr
saxon_grenadiers_descr_short

saxon_grenadiergarde
saxon_grenadiergarde_descr
saxon_grenadiergarde_descr_short
