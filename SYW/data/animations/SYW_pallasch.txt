  +1.0000000
19
  0   0   0  63   0   0   0   0   +0.0000000   +0.0000000   +0.0000000    -1
 97   0 101 114   0   0   0   0   +0.0952388   +0.0007521   -0.0000000     0
  0   0   0   0   0   0   0   0   +0.0234103   -0.4646048   -0.0049952     1
 97   0 101 114   0   0   0   0   +0.0231538   -0.4006242   -0.0124049     2
 32   0  32  32   0   0   0   0   +0.0000000   +0.2124624   -0.0000000     0
 76   1   0   0   0   0   0   0   -0.0002944   +0.2115581   -0.0000000     4
154   1  25  62   0   0   0   0   -0.0000615   +0.2349733   +0.0000000     5
102   0 230  62   0   0   0   0   +0.0040140   -0.0843265   -0.2039876     5
  0   0  64  63   0   0   0   0   -0.0000000   -0.2602945   -0.0697437     7
102   0 134  63   0   0   0   0   -0.0000000   -0.2600271   -0.0696721     8
205   0 172  63   0   0   0   0   +0.1786138   +0.0782277   -0.0239006     5
 51   0 211  63   0   0   0   0   +0.3022069   +0.0110891   -0.0137831    10
154   0 249  63   5   0   0   0   +0.2838357   -0.0030066   +0.0263561    11
  0   0  16  64   0   0   0   0   -0.1780245   +0.0782277   -0.0239006     5
 51   0  35  64   0   0   0   0   -0.3022069   +0.0110891   -0.0137830    13
102   0  54  64   4   0   0   0   -0.2838356   -0.0030066   +0.0263561    14
154   0  73  64   0   0   0   0   -0.0952388   +0.0007523   +0.0000000     0
205   0  92  64   0   0   0   0   -0.0234104   -0.4646048   -0.0049952    16
  0   0 112  64   0   0   0   0   -0.0231538   -0.4006242   -0.0124049    17
154   0 129  64 
34
data/animations/horse_cavalry_pallash/Cav~01~Idle.cas                             0      0       +0.0000   +0.0000   +0.0000         0 +0.0000    0   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  0   0    
data/animations/horse_cavalry_pallash/Cav~01~various~idle01.cas                   0      0       +0.0000   +0.0000   +0.0000         0 +0.0000    0   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  1   
 1  0  0  0  9  0 11  0 ANIM_IDLE_CLOTH           0  1  0    
data/animations/horse_cavalry_pallash/Cav~01~various~idle02.cas                   0      0       +0.0000   +0.0000   +0.0000         0 +0.0000    0   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  1   
 1  0  0  0 10  0 15  0 ANIM_IDLE_CLOTH           0  1  0    
data/animations/horse_cavalry_pallash/Cav~01~various~idle03.cas                   0      0       +0.0000   +0.0000   +0.0000         0 +0.0000    0   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  2   
 1  0  0  0  7  0 10  0 ANIM_IDLE_CLOTH           0  1  0 
 5  0  0  0 18  0 18  0 neck_crack                0  0  0    0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
data/animations/horse_cavalry_pallash/Cav~01~Idle.cas                             0      0       +0.0000   +0.0000   +0.0000         0 +0.0000    0   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  0      
data/animations/horse_cavalry_pallash/Cav~16~bowman~sitting~ready~.cas            0      0       +0.0000   +0.0000   +0.0000         0 +0.0000    0   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  0   0 0 0 0 0 0 0 0 0 0 0 0 0 0    
data/animations/horse_cavalry_pallash/Cav~151~Sitting~idle~to~bowman~sitting~ready~sr.cas  0      0       +0.0000   +0.0000   +0.0000         0 +0.0000    0   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  0   0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0    
data/animations/horse_cavalry_pallash/Cav~38~bowman~slash~left~~downwards.cas     0      0       -1.0500   -0.2900   +0.4530    -13390 +1.1435   13   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  2   
 1  0  0  0 16  0 16  0 ANIM_SWOOSH               0  0  0 
 1  0  0  0 18  0 18  0 ANIM_KILL_MOUNT           0  0  0    
data/animations/horse_cavalry_pallash/Cav~39~bowman~thurst~left~~downwards.cas    0      0       -1.2920   -0.3230   +0.5032    -13047 +1.3865   10   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  3   
 4  0  0  0 10  0 12  0 INDIVIDUAL_ATTACK_GRUNT   0  1  0 
 1  0  0  0 14  0 14  0 ANIM_SWOOSH               0  0  0 
 1  0  0  0 16  0 18  0 ANIM_SCRAPE               0  1  0    0 0 
data/animations/horse_cavalry_pallash/Cav~36~bowman~slash~left~upwards.cas        0      0       -1.2840   +0.4220   +0.4353    -13510 +1.3558    9   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  2   
 1  0  0  0 16  0 16  0 ANIM_SWOOSH               0  0  0 
 1  0  0  0 18  0 20  0 ANIM_SCRAPE               0  1  0    
data/animations/horse_cavalry_pallash/Cav~37~bowman~thurst~left~upwards.cas       0      0       -1.3000   +0.4100   +0.5129    -12980 +1.3975   12   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  2   
 1  0  0  0  8  0  8  0 ANIM_SCRAPE               0  0  0 
 1  0  0  0 10  0 12  0 ANIM_SCRAPE               0  1  0    0 0 
data/animations/horse_cavalry_pallash/Cav~30~bowman~slash~~right~downwards.cas    0      0       +1.1500   -0.5000   +0.1387     15479 +1.1583    9   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  2   
 1  0  0  0 14  0 14  0 ANIM_SWOOSH               0  0  0 
 1  0  0  0 16  0 18  0 ANIM_SCRAPE               0  1  0    
data/animations/horse_cavalry_pallash/Cav~31~bowman~thurst~right~downwards.cas    0      0       +0.3500   -0.3500   +1.5553      2470 +1.5942    8   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  3   
 4  0  0  0 10  0 12  0 INDIVIDUAL_ATTACK_GRUNT   0  1  0 
 1  0  0  0 14  0 14  0 ANIM_SWOOSH               0  0  0 
 1  0  0  0 16  0 18  0 ANIM_SCRAPE               0  1  0    
data/animations/horse_cavalry_pallash/Cav~25~bowman~slash~right.cas               0      0       +1.5750   +0.0100   +0.1185     15611 +1.5794    9   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  2   
 1  0  0  0 16  0 16  0 ANIM_SWOOSH               0  0  0 
 1  0  0  0 18  0 20  0 ANIM_SCRAPE               0  1  0    
data/animations/horse_cavalry_pallash/Cav~26~bowman~thrust~sword~~right.cas       0      0       +0.3550   +0.3230   +1.5576      2408 +1.5975    9   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  2   
 1  0  0  0 24  0 24  0 ANIM_SWOOSH               0  0  0 
 1  0  0  0 26  0 28  0 ANIM_SCRAPE               0  1  0    
data/animations/horse_cavalry_pallash/Cav~28~bowman~slash~to~~right~upward.cas    0      0       +1.5990   +0.4120   +0.0998     15733 +1.6021    9   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  2   
 1  0  0  0 16  0 16  0 ANIM_SWOOSH               0  0  0 
 1  0  0  0 18  0 20  0 ANIM_SCRAPE               0  1  0    
data/animations/horse_cavalry_pallash/Cav~29~bowman~thurst~~~right~upward.cas     0      0       +0.3450   +0.5500   +1.5618      2284 +1.5994    8   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  2   
 1  0  0  0 22  0 22  0 ANIM_SWOOSH               0  0  0 
 1  0  0  0 24  0 26  0 ANIM_SCRAPE               0  1  0    
data/animations/horse_cavalry_pallash/Cav~27~bowman~overhead~chop~~~right.cas     0      0       +1.2800   +0.5330   +0.7244     11483 +1.4708    8   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  2   
 1  0  0  0 16  0 16  0 ANIM_SWOOSH               0  0  0 
 1  0  0  0 18  0 20  0 ANIM_SCRAPE               0  1  0    
data/animations/horse_cavalry_pallash/Cav~32~bowman~overhead~chop~~right~downwards.cas  0      0       +0.9820   -0.5020   +0.8342     10663 +1.2885   10   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  3   
 4  0  0  0  8  0 10  0 INDIVIDUAL_ATTACK_GRUNT   0  1  0 
 1  0  0  0 10  0 10  0 ANIM_SWOOSH               0  0  0 
 1  0  0  0 12  0 14  0 ANIM_SCRAPE               0  1  0    0 0 0 0 
data/animations/horse_cavalry_pallash/Cav~die.cas                                 0  52359       +0.0000   +0.0000   +0.0000         0 +0.0000    0   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  2   
 4  0  0  0  2  0  2  0 INDIVIDUAL_DEATH          0  0  0 
 2  0  0  0 16  0 16  0 fall                      0  0  0    0 
data/animations/horse_cavalry_pallash/Cav~die.cas                                 0  52359       +0.0000   +0.0000   +0.0000         0 +0.0000    0   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  2   
 4  0  0  0  2  0  2  0 INDIVIDUAL_DEATH          0  0  0 
 2  0  0  0 16  0 16  0 fall                      0  0  0    0 
data/animations/horse_cavalry_pallash/Cav~die.cas                                 0  52359       +0.0000   +0.0000   +0.0000         0 +0.0000    0   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  2   
 4  0  0  0  2  0  2  0 INDIVIDUAL_DEATH          0  0  0 
 2  0  0  0 16  0 16  0 fall                      0  0  0    0 0 0 
data/animations/Flailing~Cycle.cas                                                0      0       +0.0000   +0.0000   +0.0000         0 +0.0000    0   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  1   
 4  0  0  0  0  0  2  0 Fall_Scream               0  1  1    
data/animations/Flailing~Cycle~to~land.cas                                        0  22126       +0.0000   +0.0000   +0.0000         0 +0.0000    0   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  2   
 1  0  0  0  2  0  2  0 ANIM_FALL_SQUASH          0  0  0 
 4  0  0  0  4  0  4  0 INDIVIDUAL_FALL_GRUNT     0  0  0    0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
data/animations/horse_cavalry_pallash/general_signal_charge.cas                   0      0       +0.0000   +0.0000   +0.0000         0 +0.0000    0   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  0      
data/animations/horse_cavalry_pallash/general_signal_move.cas                     0      0       +0.0000   +0.0000   +0.0000         0 +0.0000    0   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  0      
data/animations/horse_cavalry_pallash/general_signal_reform.cas                   0      0       +0.0000   +0.0000   +0.0000         0 +0.0000    0   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  0      
data/animations/horse_cavalry_pallash/general_signal_halt.cas                     0      0       +0.0000   +0.0000   +0.0000         0 +0.0000    0   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  0   0 0 0 0 0 0 0 0 0 0    
data/animations/horse_cavalry_pallash/CAV_165_sword_guard_left.CAS                0      0       +0.0000   +0.0000   +0.0000         0 +0.0000    0   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  0      
data/animations/horse_cavalry_pallash/CAV_166_sword_guard_right.CAS               0      0       +0.0000   +0.0000   +0.0000         0 +0.0000    0   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  0      
data/animations/horse_cavalry_pallash/CAV_167_sword_dodge_left.CAS                0      0       +0.0000   +0.0000   +0.0000         0 +0.0000    0   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  0      
data/animations/horse_cavalry_pallash/CAV_166_sword_dodge_right.CAS               0      0       +0.0000   +0.0000   +0.0000         0 +0.0000    0   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  0      
data/animations/horse_cavalry_pallash/CAV_169_sword_feint_left.CAS                0      0       +0.0000   +0.0000   +0.0000         0 +0.0000    0   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  0      
data/animations/horse_cavalry_pallash/CAV_170_sword_feint_right.CAS               0      0       +0.0000   +0.0000   +0.0000         0 +0.0000    0   0  +0.0000      1820   32586 +0.0000  +0.0000  +1.0000 
  0      0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
  +0.0000000   +0.0000000   +0.0000000 0 0 
