;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;
;	This file contains the mount linkages; it gets parsed on application 
;	startup but is not otherwise referred to. The data format is thus:
;
;	Fields should be entered in the order shown.
;
;	;						indicates a comment ;-)
;	/ /						indicates a section
;	[]						indicates optional
;
;	/generic/
;							
;	type					indicates a new mount type, must be followed by id name string
;	class					mount class (current possibilities - horse, camel, elephant, chariot, scorpion_cart)
;	model*					model id from descr_model_battle 
;	radius					mount radius
;	[ x_radius ]			mount x axis radius for elliptical mounts (radius is y radius in this case) 
;	height					mount height
;	mass					mount mass
;	banner_height			height of banners above mount
;	bouyancy_offset			bouyancy offset of mount above root node
;	water_trail_effect		display effect for moving through water
;
;	* do not specify for chariots and scorpion carts - use lods in the chariot/scorpion cart specific section
;
;	/horse, camel or elephant specific/
;
;	root_node_height		height of the horse, camel or elephants root node above the ground
;
;	/horse and camel specific/
;	
;	rider_offset			(x, y, z) for the rider relative to horse or camel root node
;
;	/elephant, chariot and scorpion cart specific/
;
;	attack_delay			delay between mount attacks (tusks and scythes) in seconds
;
;	/elephant specific/
;	
;	dead_radius				radius of dead obstacle
;	tusk_z					distance along the z axis of tusks from centre
;	tusk_radius				radius of tusk attack
;	riders					number of riders
;	rider_offset			(x, y, z) for each rider relative to elephant root node
;
;	/chariot and scorpion cart specific/
;
;	axle_width				width of the chariot/scorpion cart axle
;	wheel_radius			radius of the chariots/scorpion cart wheels
;	pivot_offset			(x, y, z) position of the tow-pole pivot point releative to the axle centre
;	pole_length				length of the tow pole
;	pole_pivot				offset from pole origin to pivot point
;	pole_connect			height at which pole connects to harness above ground
;	harness_connect			height at which harness connects to horse above ground
;	[
;		attack_delay			delay between scythe attacks in seconds
;		scythe_radius			radius of scythe from end of axle
;		revs_per_attack 		revolutions per second per scythe attack on an individual (rounded down)	
;	] #
;	horse_type				type of horse to tow chariot/scorpion cart
;	horses					number of horses
;	horse_offset			(x, z) for each horse relative to pivot point
;	riders					number of riders 
;	rider_offset			(x, y, z) for each rider releative to axle centre
;	lods					number of lods - min 1, max 5
;	lod						(model_filename, range) for each lod in ascending order of distance (use range = max for furthest lod)
;	trail_effect			effect definition for water trails
;
;	# ommitting this section will mean no scythes are displayed and no scythe attacks will be used.  
;
;	/scorpion cart specific/
;
;	scorpion_offset		
;	scorpion_height			
;	scorpion_forward_length
;	scorpion_reload_ticks
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;
; camels 
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

type				camel
class				camel
model				camel
radius				1.5
x_radius			0.57	;changed from 0.5 29/4/05
height				1.2
mass				3
banner_height		0
bouyancy_offset		1.8
water_trail_effect  camel_water_trail
root_node_height	1.52
rider_offset		0, 0.43, -0.2

type				camel cataphract
class				camel
model				camel_cataphract
radius				1.5
x_radius			0.57
height				1.2
mass				3.2
banner_height		0
bouyancy_offset		1.8
water_trail_effect	camel_water_trail
root_node_height	1.52
rider_offset		0, 0.43, -0.2

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;
; horses 
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

type				horse cataphract
class				horse
model				horse_cataphract
radius				1.5
x_radius			0.5	;changed from 0.5 29/4/05
height				1.2
mass				6
banner_height		0
bouyancy_offset		1.8
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.3, 0.0

type				light horse
class				horse
model				horse_light
radius				1.5
x_radius			0.5	;changed from 0.5 29/4/05
height				1.2
mass				4.5
banner_height		0
bouyancy_offset		1.6
water_trail_effect	horse_water_trail
root_node_height	1.4
rider_offset		0.0, 0.15, 0.0


type				hussar horse
class				horse
model				hussar_horse
radius				1.5
x_radius			0.5	;changed from 0.5 29/4/05
height				1.2
mass				3.6
banner_height		0
bouyancy_offset		1.8
water_trail_effect	horse_water_trail
root_node_height	1.4
rider_offset		0.0, 0.15, 0.2


type				medium horse
class				horse
model				horse_medium
radius				1.0
x_radius			0.5	;changed from 0.5 29/4/05
height				1.2
mass				4	;changed from 4.0 29/4/05
banner_height		0
bouyancy_offset		1.8
water_trail_effect	horse_water_trail
root_node_height	1.5
rider_offset		0.0, 0.15, 0.0

type				heavy horse
class				horse
model				horse_heavy
radius				1.5
x_radius			0.5	;changed from 0.5 29/4/05
height				1.2
mass				3.2
banner_height		0
bouyancy_offset		1.8
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.15, 0.0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Mughul;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

type				mughul horse
class				horse
model				mughul_horse
radius				1.5
x_radius			0.35	;changed from 0.5 29/4/05
height				2.3
mass				3.2
banner_height		0
bouyancy_offset		1.57
water_trail_effect	horse_water_trail
root_node_height	1.5
rider_offset		0.0, 0.2, 0.1

type				mughul sphorse
class				horse
model				mughul_heavy_horse
radius				1.5
x_radius			0.35	;changed from 0.5 29/4/05
height				2.3
mass				3.2
banner_height		0
bouyancy_offset		1.57
water_trail_effect	horse_water_trail
root_node_height	1.5
rider_offset		0.0, 0.2, 0.1


type				CA horse
class				horse
model				CA_horse
radius				1.5
x_radius			0.35	;changed from 0.5 29/4/05
height				2.7
mass				3
banner_height		0
bouyancy_offset		1.2
water_trail_effect	horse_water_trail
root_node_height	1.3
rider_offset		0.0, 0.2, 0.1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Austria;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

type				austrian horse
class				horse
model				austrian_horse
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				3.2
banner_height		0
bouyancy_offset		1.6
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.2, 0.15

type				austrian dragoon horse
class				horse
model				austrian_dragoon_horse
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				3.2
banner_height		0
bouyancy_offset		1.5
water_trail_effect	horse_water_trail
root_node_height	1.5
rider_offset		0.0, 0.2, 0.15

type				horse austrian stabsdrag
class				horse
model				horse_austrian_stabsdrag
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				3.2
banner_height		0
bouyancy_offset		1.6
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.2, 0.15

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Denmark;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

type				danish horse
class				horse
model				danish_horse
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				3.2
banner_height		0
bouyancy_offset		1.6
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.2, 0.15


type				danish dragoon horse
class				horse
model				danish_dragoon_horse
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				3.2
banner_height		0
bouyancy_offset		1.6
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.2, 0.15

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Wurttemberg;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

type				wurt dragoon horse
class				horse
model				horse_wurttemberg_dragoon
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				3.2
banner_height		0
bouyancy_offset		1.6
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.2, 0.15

type				wurt cuir horse
class				horse
model				horse_wurttemberg_cuir
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				3.2
banner_height		0
bouyancy_offset		1.6
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.2, 0.15

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;France;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

type				french cuirassier horse
class				horse
model				french_cuirassier_horse
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				1.2
mass				5
banner_height		0
bouyancy_offset		1.8
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.2, 0.1


type				french gendarme horse
class				horse
model				french_gendarme_horse
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				1.2
mass				5.5
banner_height		0
bouyancy_offset		1.8
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.2, 0.1


type				french garde horse
class				horse
model				french_garde_horse
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				1.2
mass				5.5
banner_height		0
bouyancy_offset		1.8
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.2, 0.1


type				french dragoon horse
class				horse
model				french_dragoon_horse
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				1.2
mass				4
banner_height		0
bouyancy_offset		1.8
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.2, 0.1


type				french ADC horse
class				horse
model				french_ADC_horse
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				1.2
mass				4
banner_height		0
bouyancy_offset		1.8
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.2, 0.1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Savoy;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

type				horse savoyard dragoon
class				horse
model				horse_savoyard_dragoon
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				1.2
mass				4
banner_height		0
bouyancy_offset		1.8
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.2, 0.1


type				horse savoyard horse
class				horse
model				horse_savoyard_horse
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				1.2
mass				4.4
banner_height		0
bouyancy_offset		1.8
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.2, 0.1


type				horse savoyard gdcorps
class				horse
model				horse_savoyard_gdcorps
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				1.2
mass				4.4
banner_height		0
bouyancy_offset		1.8
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.2, 0.1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Spain;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

type				horse spanish horseguard
class				horse
model				horse_spanish_horseguard
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				1.2
mass				5.5
banner_height		0
bouyancy_offset		1.8
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.2, 0.1


type				horse spanish horse
class				horse
model				horse_spanish_horse
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				1.2
mass				5.5
banner_height		0
bouyancy_offset		1.8
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.2, 0.1


type				horse spanish dragoon
class				horse
model				horse_spanish_dragoon
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				1.2
mass				4.5
banner_height		0
bouyancy_offset		1.8
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.2, 0.1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Portugal;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


type				horse portuguese dragoon
class				horse
model				horse_portuguese_dragoon
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				1.2
mass				3.5
banner_height		0
bouyancy_offset		1.8
water_trail_effect	horse_water_trail
root_node_height	1.45
rider_offset		0.0, 0.2, 0.1


type				horse portuguese cavalry
class				horse
model				horse_portuguese_cavalry
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				1.2
mass				4
banner_height		0
bouyancy_offset		1.8
water_trail_effect	horse_water_trail
root_node_height	1.45
rider_offset		0.0, 0.2, 0.1


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Brunswick;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


type				horse brunswick dragoon
class				horse
model				horse_brunswick_dragoon
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				3.2
banner_height		0
bouyancy_offset		1.6
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.2, 0.15


type				horse brunswick horse
class				horse
model				horse_brunswick_horse
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				4.5
banner_height		0
bouyancy_offset		1.6
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.2, 0.15


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Bavaria;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	


type				bavarian horse
class				horse
model				bavarian_horse
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.0
mass				5.0
banner_height		0
bouyancy_offset		1.4
water_trail_effect	horse_water_trail
root_node_height	1.4
rider_offset		0.0, 0.2, 0.15


type				bavarian dhorse
class				horse
model				bavarian_dhorse
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				1.9
mass				4.5
banner_height		0
bouyancy_offset		1.32
water_trail_effect	horse_water_trail
root_node_height	1.32
rider_offset		0.0, 0.2, 0.15

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Saxony;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	


type				horse sax CL
class				horse
model				horse_sax_CLeger
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				1.8
mass				4.2
banner_height		0
bouyancy_offset		1.26
water_trail_effect	horse_water_trail
root_node_height	1.26
rider_offset		0.0, 0.25, 0.15


type				horse sax uhlan
class				horse
model				horse_sax_uhlan
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				1.69
mass				3.8
banner_height		0
bouyancy_offset		1.18
water_trail_effect	horse_water_trail
root_node_height	1.18
rider_offset		0.0, 0.25, 0.15

type				horse sax cuirassier
class				horse
model				horse_sax_cuirassier
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.0
mass				5.5
banner_height		0
bouyancy_offset		1.4
water_trail_effect	horse_water_trail
root_node_height	1.4
rider_offset		0.0, 0.25, 0.15


type				horse sax carab
class				horse
model				horse_sax_carab
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.0
mass				5.8
banner_height		0
bouyancy_offset		1.4
water_trail_effect	horse_water_trail
root_node_height	1.4
rider_offset		0.0, 0.25, 0.15


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Britain;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

type				horse hanovarian dragoon
class				horse
model				horse_hanovarian_dragoon
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				3.2
banner_height		0
bouyancy_offset		1.6
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.2, 0.15


type				horse hanovarian horse
class				horse
model				horse_hanovarian_horse
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				4
banner_height		0
bouyancy_offset		1.4
water_trail_effect	horse_water_trail
root_node_height	1.4
rider_offset		0.0, 0.2, 0.1


type				horse hanovarian garde
class				horse
model				horse_hanovarian_garde
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				4
banner_height		0
bouyancy_offset		1.4
water_trail_effect	horse_water_trail
root_node_height	1.4
rider_offset		0.0, 0.2, 0.1


type				ADC horse UK
class				horse
model				ADC_horse_UK
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				4.5
banner_height		0
bouyancy_offset		1.4
water_trail_effect	horse_water_trail
root_node_height	1.4
rider_offset		0.0, 0.2, 0.1



type				british horse 1
class				horse
model				british_horse_1
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				4.5
banner_height		0
bouyancy_offset		1.4
water_trail_effect	horse_water_trail
root_node_height	1.4
rider_offset		0.0, 0.2, 0.1


type				british horse 2
class				horse
model				british_horse_2
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				4.5
banner_height		0
bouyancy_offset		1.4
water_trail_effect	horse_water_trail
root_node_height	1.4
rider_offset		0.0, 0.2, 0.1

type				horse dragoon
class				horse
model				horse_british_dragoon
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				4
banner_height		0
bouyancy_offset		1.4
water_trail_effect	horse_water_trail
root_node_height	1.4
rider_offset		0.0, 0.2, 0.1

type				british horse 3
class				horse
model				british_horse_3
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				4
banner_height		0
bouyancy_offset		1.4
water_trail_effect	horse_water_trail
root_node_height	1.4
rider_offset		0.0, 0.2, 0.1


type				british horse 4
class				horse
model				british_horse_4
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				4
banner_height		0
bouyancy_offset		1.4
water_trail_effect	horse_water_trail
root_node_height	1.4
rider_offset		0.0, 0.2, 0.1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Netherlands;;;;;;;;;;;;;;;;;


type				horse dutch dragoon
class				horse
model				horse_dutch_dragoon
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				4
banner_height		0
bouyancy_offset		1.4
water_trail_effect	horse_water_trail
root_node_height	1.4
rider_offset		0.0, 0.2, 0.1


type				horse dutch horse
class				horse
model				horse_dutch_horse
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				4.5
banner_height		0
bouyancy_offset		1.4
water_trail_effect	horse_water_trail
root_node_height	1.4
rider_offset		0.0, 0.2, 0.1


type				horse dutch gtpaard
class				horse
model				horse_dutch_gtpaard
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				4.5
banner_height		0
bouyancy_offset		1.4
water_trail_effect	horse_water_trail
root_node_height	1.4
rider_offset		0.0, 0.2, 0.1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Sweden;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	


type				swedish horse 1
class				horse
model				swedish_horse_1
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				4.5
banner_height		0
bouyancy_offset		1.6
water_trail_effect	horse_water_trail
root_node_height	1.4
rider_offset		0.0, 0.2, 0.1


type				swedish horse 2
class				horse
model				swedish_horse_2
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				3.2
banner_height		0
bouyancy_offset		1.6
water_trail_effect	horse_water_trail
root_node_height	1.35
rider_offset		0.0, 0.2, 0.1


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Russia;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

type				russian horse 1
class				horse
model				russian_horse_1
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				3.0
mass				3.2
banner_height		0
bouyancy_offset		1.8
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.2, 0.1


type				russian horse 2
class				horse
model				russian_horse_2
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				3.0
mass				3.2
banner_height		0
bouyancy_offset		1.8
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.2, 0.1

type				russian horse 3
class				horse
model				russian_horse_3
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				3.0
mass				3.2
banner_height		0
bouyancy_offset		1.8
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.2, 0.1


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Hesse-Kassel;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

type				horse hessian horse
class				horse
model				horse_hessian_horse
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.3
mass				4
banner_height		0
bouyancy_offset		1.4
water_trail_effect	horse_water_trail
root_node_height	1.4
rider_offset		0.0, 0.2, 0.1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Prussia;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	


type				prussian horse 1
class				horse
model				prussian_horse_1
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.5
mass				5
banner_height		0
bouyancy_offset		1.6
water_trail_effect	horse_water_trail
root_node_height	1.4
rider_offset		0.0, 0.2, 0.1


type				prussian horse 2
class				horse
model				prussian_horse_2
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.5
mass				4
banner_height		0
bouyancy_offset		1.6
water_trail_effect	horse_water_trail
root_node_height	1.38
rider_offset		0.0, 0.2, 0.15


type				prussian horse 3
class				horse
model				prussian_horse_3
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.5
mass				4
banner_height		0
bouyancy_offset		1.6
water_trail_effect	horse_water_trail
root_node_height	1.38
rider_offset		0.0, 0.2, 0.15


type				generals horse
class				horse
model				generals_horse
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				1.2
mass				3.2
banner_height		0
bouyancy_offset		1.8
water_trail_effect	horse_water_trail
root_node_height	1.6
rider_offset		0.0, 0.15, 0.0

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Poland;;;;;;;;;;;;;;;;;;;;

type				polish pancerni horse
class				horse
model				polish_pancerni_horse
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				1.9
mass				5.0
banner_height		0
bouyancy_offset		1.3
water_trail_effect	horse_water_trail
root_node_height	1.3
rider_offset		0.0, 0.2, 0.15


type				polish dragoon horse
class				horse
model				polish_dragoon_horse
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				1.9
mass				5.0
banner_height		0
bouyancy_offset		1.3
water_trail_effect	horse_water_trail
root_node_height	1.3
rider_offset		0.0, 0.2, 0.15


type				polish hussar horse
class				horse
model				polish_hussar_horse
radius				1.23
x_radius			0.3	;changed from 0.5 29/4/05
height				2.0
mass				6.0
banner_height		0
bouyancy_offset		1.4
water_trail_effect	horse_water_trail
root_node_height	1.4
rider_offset		0.0, 0.2, 0.15

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;
; elephants 
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

; the model in the game for a forest elephant is currently african, these values match the model not the description

type				elephant forest
class				elephant
model				elephant_forest
radius				4.4
x_radius			1.0
height				1.2
mass				12
banner_height		0
bouyancy_offset		2.5
water_trail_effect	elephant_water_trail
root_node_height	1.8
attack_delay		1
dead_radius			2.5
tusk_z				2.25
tusk_radius			1.5
riders				2
rider_offset		0.0, 1.0, 0.75
rider_offset		0.0, 1.1, 0.05

type				elephant african
class				elephant
model				elephant_african
radius				5.5
x_radius			1.3
height				3
mass				15
banner_height		1
bouyancy_offset		3
water_trail_effect	elephant_water_trail
root_node_height	2.52
attack_delay		1
dead_radius			2.5
tusk_z				3.0
tusk_radius			2.0
riders				3
rider_offset		0.0, 1.225, 1.306
rider_offset		0.0, 1.1, 0.5
rider_offset		0, 1.1, -.25

type				elephant indian
class				elephant
model				elephant_indian
radius				5.5
x_radius			1.3
height				3
mass				15
banner_height		1
bouyancy_offset		3
water_trail_effect	elephant_water_trail
root_node_height	2.27
attack_delay		1
dead_radius			2.5
tusk_z				3.0
tusk_radius			2.0
riders				3
rider_offset		0.0, 1.225, 1.306
rider_offset		0.0, 1.1, 0.5
rider_offset		0, 1.1, -.25

type				elephant african cataphract
class				elephant
model				elephant_african_cataphract
radius				5.8
x_radius			1.5
height				4
mass				20
banner_height		1
bouyancy_offset		4
water_trail_effect	elephant_water_trail
root_node_height	2.52
attack_delay		1
dead_radius			2.5
tusk_z				3.1
tusk_radius			2.0
riders				3
rider_offset		0.0, 1.635, 1.45
rider_offset		0.0, 1.45, 0.5
rider_offset		0, 1.45, -.25

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;
; chariots 
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

type				egyptian chariot				
class				chariot
radius				6
x_radius			1.5
height				3
mass				10
banner_height		0
bouyancy_offset		0
water_trail_effect	chariot_water_trail
axle_width			1.82
wheel_radius		0.474
pivot_offset		0, 0.345, 1.244
pole_length			1.9
pole_pivot			0, 0, -0.871
pole_connect		1.63
harness_connect		1.73
attack_delay		0.25
scythe_radius		0.5
revs_per_attack		1
horse_type			medium horse
horses				2
horse_offset		-0.65, 2.057
horse_offset		0.65, 2.057
riders				3
rider_offset		0	, 0.15, 0.5
rider_offset		-0.35, 0.15, -0.1
rider_offset		0.35	, 0.15, -0.1
lods				3
lod					mount_egyptian_chariot_high.cas,	50	
lod					mount_egyptian_chariot_med.cas,		125
lod					mount_egyptian_chariot_low.cas,		250

type				heavy chariot				
class				chariot
radius				6
x_radius			1.5
height				3
mass				10
banner_height		0
bouyancy_offset		0
water_trail_effect	chariot_water_trail
axle_width			1.82
wheel_radius		0.474
pivot_offset		0, 0.352, 1.019
pole_length			2.057
pole_pivot			0, 0, -1.021
pole_connect		1.63
harness_connect		1.73
attack_delay		0.25
scythe_radius		0.5
revs_per_attack		1
horse_type			heavy horse
horses				2
horse_offset		-0.65, 2.057
horse_offset		0.65, 2.057
riders				3
rider_offset		0	, 0.15, 0.5
rider_offset		-0.5, 0.15, -0.05
rider_offset		0.5	, 0.15, -0.05
lods				3
lod					mount_greek_chariot_high.cas,	50	
lod					mount_greek_chariot_med.cas,	125
lod					mount_greek_chariot_low.cas,	250

type				barbarian heavy chariot				
class				chariot
radius				6
x_radius			1.5
height				3
mass				10
banner_height		0
bouyancy_offset		0
water_trail_effect	chariot_water_trail
axle_width			1.6
wheel_radius		0.58
pivot_offset		0, 0.133, 0.862
pole_length			2.55
pole_pivot			0, -0.056, -1.419
pole_connect		1.73
harness_connect		1.78
attack_delay		0.25
scythe_radius		0.5
revs_per_attack		1
horse_type			light horse
horses				2
horse_offset		-0.65, 2.057
horse_offset		0.65, 2.057
riders				3
rider_offset		0, 	0.15,	0.5
rider_offset		-0.5,	0.10,	-0.25
rider_offset		0.5,	0.15,	-0.05
lods				3
lod					mount_barbarian_chariot_high.cas,	50	
lod					mount_barbarian_chariot_med.cas,	125
lod					mount_barbarian_chariot_low.cas,	250

type				scythed chariot				
class				chariot
radius				6
x_radius			1.5
height				3
mass				14
banner_height		0
bouyancy_offset		0
water_trail_effect	chariot_water_trail
axle_width			1.82
wheel_radius		0.474
pivot_offset		0, 0.345, 1.193
pole_length			2.05
pole_pivot			0, 0, -1.021
pole_connect		1.63
harness_connect		1.73
attack_delay		0.25
scythe_radius		0.5
revs_per_attack		1
horse_type			heavy horse
horses				2
horse_offset		-0.65, 2.057
horse_offset		0.65, 2.057
riders				3
rider_offset		0	, 0.15, 0.5
rider_offset		-0.5, 0.15, -0.05
rider_offset		0.5	, 0.15, -0.05
lods				3
lod					mount_greek_scythed_chariot_high.cas,	50	
lod					mount_greek_scythed_chariot_med.cas,	125
lod					mount_greek_scythed_chariot_low.cas,	250

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;
; scorpion carts
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	

type					scoprion cart				
class					scorpion_cart
radius					6
x_radius				1.5
height					3
mass					10
banner_height			0
bouyancy_offset			0
water_trail_effect		chariot_water_trail
axle_width				1.82
wheel_radius			0.574
pivot_offset			0, 0.345, 1.244
pole_length				1.9
pole_pivot				0, 0, -0.871
pole_connect			1.63
harness_connect			1.73
horse_type				heavy horse
horses					2
horse_offset			-0.65, 2.057
horse_offset			0.65, 2.057
riders					2
rider_offset			0	, 0.15, 0.5
rider_offset			0, 0, 0
lods					1
lod						scorpion_cart.cas,	50	
scorpion_offset			0, 0.15, -0.5
scorpion_height			1.165		
scorpion_forward_length	1
scorpion_reload_ticks	50

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
