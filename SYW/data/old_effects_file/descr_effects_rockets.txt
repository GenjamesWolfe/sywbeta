;-----------------------------------------------------------------------------
; Rocket trail effects.
;-----------------------------------------------------------------------------


effect rocket
{
	type projectile
	{
		model		models_effects/flaming_arrow.cas
		fade_time	0.01
	}
}

effect rocket_light
{
	type light
	{
		fade_out_time	0.1
		keyframe_colour
		{
				0.05, 	240, 100, 0
				0.1, 	240, 130, 0
				0.15, 	240, 80, 0
				0.2, 	240, 140, 0
				0.25, 	240, 70, 0
				0.3, 	240, 120, 0
		}
			
		keyframe_radius
		{
				0.05, 	10
				0.1, 	11
		}
	}
}

effect rocket_explosion_light
{
	type light
	{
		offset		0.0, 0.0, 5.0
		fade_in_time	0.1
		fade_out_time	0.1
		keyframe_colour
		{
				0.05, 	240, 100, 0
				0.1, 	240, 130, 0
				0.15, 	240, 80, 0
				0.2, 	240, 140, 0
				0.25, 	240, 70, 0
				0.3, 	240, 120, 0
		}
			
		keyframe_radius
		{
				0.05, 	20
				0.1, 	21
		}
	}
}


effect rocket_smoke
{
	type particle
 	{
		texture			battlefield/fire/smoke0.tga

		offset			0.0, 0.0, 0.0
		size_range      	1.0, 2.5
		initial_size		0.5, 1.5
		age_range      	 	1.0, 1.5
	    	grow_range		1.5, 1.8
		velocity   		0.0, 0.2
		colour     		200, 200, 200
		alpha_min		80
		alpha_max	   	100			 ; maximum alpha value
		batched			
		emitter distance
		{
			density			3 	     ; how many particels per meter
		}
		
		fade_time		0.1		

		clr_adjust_by_ambient_intensity		
	}
}

effect exploding_shell2
{
	type explosion
	{
		; fireball
		model 				../SYW/data/models_effects/ntw_shell_explosion.cas
		anim_time			1
		max_scale   			1.0
		max_particle_scale 		50
		scale_time_bias			0.999
		alpha_time_bias			0.3
		

		; debris
        debris_model                             models_effects/debri02.cas
        debris_num_particles     0
        debris_lifetime_range    2.2, 20
        debris_emit_angle        70              ; degrees
        debris_time_bias         0.9
        debris_velocity_range    5.5, 30.5
        debris_scale_range       0.4, 1.5
	}
}


effect rocket_explosion
{
	type explosion
	{
		; main explosion
		model 				models_effects/explosion.cas
		anim_time			5
		max_scale   			3
		max_particle_scale 		5
		scale_time_bias			.9
		alpha_time_bias			0.6		
		

		; debris
        debris_model                             models_effects/fire_debri_01.CAS
        debris_num_particles     15
        debris_lifetime_range    15, 45
        debris_emit_angle        180               ; degrees
        debris_time_bias         0.0
        debris_velocity_range    6, 18
        debris_scale_range       1.4, 3.6
	}
}

effect rocket_dust_explosion
{
	type explosion
	{
	; dust explode
		model 				models_effects/explosion_dust.cas
		anim_time			1.7
		max_scale   			3
		max_particle_scale 		5
		scale_time_bias			.9
		alpha_time_bias			0.6

		; debris
        debris_model                             models_effects/fire_debri_02.CAS
        debris_num_particles     0
        debris_lifetime_range    2.2, 15
        debris_emit_angle        50               ; degrees
        debris_time_bias         0.9
        debris_velocity_range    10.5, 15.5
        debris_scale_range       1.4, 4.6
	}
}

effect rocket_dust_explosion2
{
	type explosion
	{
	; dust explode
		model 				models_effects/explosion_dust2.cas
		anim_time			9
		max_scale   			6
		max_particle_scale 		10
		scale_time_bias			.9
		alpha_time_bias			0.6

		; debris
	        debris_model                             models_effects/debri02.cas
        	debris_num_particles     0
	        debris_lifetime_range    2.2, 20
        	debris_emit_angle        70              ; degrees
	        debris_time_bias         0.9
        	debris_velocity_range    5.5, 30.5
	        debris_scale_range       0.4, 1.5
	}
}

effect rocket_dust_explosion3
{
	type explosion
	{
	; dust explode
		model 				models_effects/flame_explode.cas
		anim_time			.7
		max_scale   			1.5
		max_particle_scale 		2.5
		scale_time_bias			.9
		alpha_time_bias			0.9

		; debris
        debris_model                             models_effects/fire_debri_02.CAS
        debris_num_particles     0
        debris_lifetime_range    2.2, 15
        debris_emit_angle        50               ; degrees
        debris_time_bias         0.9
        debris_velocity_range    10.5, 15.5
        debris_scale_range       1.4, 4.6
	}
}

effect_set rocket_set
{
	lod 1000
	{
		rocket
		rocket_smoke
		rocket_light
	}
}

effect_set rocket_end_effect
{ 
	lod 1000 
	{ 
		rocket_dust_explosion3
		rocket_explosion
		rocket_dust_explosion
		rocket_dust_explosion2
	} 
}